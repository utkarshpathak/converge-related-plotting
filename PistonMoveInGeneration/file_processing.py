from pandas import read_excel
#install pandas for this as we need to read xslx files.

mysheet = "CrankAngleVsZValue" #the sheet in the excel file
fileName = "PressurePosition125bar.xlsx" #file's name

df = read_excel(fileName, sheet_name=mysheet)

n = len(df)

f = open("piston_move_test.in", "w")
f.write("TEMPORAL\n")
f.write("SEQUENTIAL\n")
f.write("crank          x              y              z\n")
for i in range(n):
    f.write(str(round(df.iat[i,0], 2)))
    if len(str(round(df.iat[i,0], 2)))==5:
        f.write("          ")
    else:
        f.write("         ")
    f.write(str(0.0))
    f.write("            ")
    f.write(str(0.0))
    f.write("            ")
    f.write(str(round(df.iat[i,1], 7)))
    f.write("\n")
f.close()