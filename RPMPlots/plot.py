from pandas import read_excel
from matplotlib import pyplot as plt

plt.rcParams["figure.figsize"] = (7,5)

mysheet = "CrankAngleVsZValue" #the sheet in the excel file
fileName = "PressurePosition125bar.xlsx" #file's name

df = read_excel(fileName, sheet_name=mysheet)

crank_angle_experimental = df.iloc[:,0].values
pressure_experimental = df.iloc[:,2].values

plt.plot(crank_angle_experimental, pressure_experimental, '-', color='black', label='Experiment')

#-------------------------------------------------------------------------------------------------
# sim 2

crank_angle_simulation2 = []
pressure_simulation2 = []

line_no = 0

with open("thermo2_region0.out", "r") as f:
    for lines in f:
        line_no +=1
        if line_no < 6:
            continue
        lines = lines.split()
        crank_angle_simulation2.append(float(lines[0]))
        pressure_simulation2.append(10.0*float(lines[1]))
    

plt.plot(crank_angle_simulation2, pressure_simulation2, '-', color='blue', label='Simulation (RPM 585)')

#-------------------------------------------------------------------------------------------------

#-------------------------------------------------------------------------------------------------
# sim 6

crank_angle_simulation6 = []
pressure_simulation6 = []

line_no = 0

with open("thermo6_region0.out", "r") as f:
    for lines in f:
        line_no +=1
        if line_no < 6:
            continue
        lines = lines.split()
        crank_angle_simulation6.append(float(lines[0]))
        pressure_simulation6.append(10.0*float(lines[1]))
    

plt.plot(crank_angle_simulation6, pressure_simulation6, '-', color='red', label='Simulation (RPM 501)')

#-------------------------------------------------------------------------------------------------


#Rest of the Plotting
plt.xlabel("Crank Angle (deg.)", fontsize=16)
plt.ylabel("Pressure (bar)", fontsize=16)
plt.title("Pressure Trace (at OP4 - 125 bar, 920 K)",fontsize=16,horizontalalignment='center') #change
plt.axis([530.0,730.0,0.0,220.0]) #change
plt.grid()
plt.legend()

#show()
plt.savefig('PressureTrace.png', bbox_inches='tight')
